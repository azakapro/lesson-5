// 1st
function checkVowels(str){
    let count = 0;
    let vowels=("aeiouAEIOU")
    for (let i=0; i<str.length; i++){
    if (vowels.includes(str[i])){
        count++
    }
  }
  return count
}

// // 1.1 version
// function checkVowels(str) {
//     var vowels = ["a", "e", "i", "o", "u"];
//     return str.split("").reduce((acc, curr) => {
//       if (vowels.includes(curr)) {
//         return acc + 1;
//       }
//       return acc;
//     }, 0);
// }
// console.log(checkVowels("The quick brown fox")) 


// 2nd
function dayCount(){
    today = new Date()
    var newYear = new Date(today.getFullYear(), 11, 31)
    if (today.getMonth()==11 && today.getDate()>31){
        newYear.setFullYear(newYear.getFullYear()+1)
    }
    var day = 1000*60*60*24
    console.log(Math.ceil((newYear.getTime()-today.getTime())/(day)))
}

// 3rd
function symb(arr3){
symbols=('~!@#$%^&*()_+/*-.,/[]{}|:";')
for (let i=0; i<arr3.length; i++){
    if (symbols.includes(arr3[i])){
        console.log("Found " + arr3[i])
    }
  }
}

// 4th
function intersect(arr40, arr41){
    let arr42 =[]
    for(let i=0; i<arr40.length; i++){
        if(arr40.includes(arr41[i])){
            arr42.push(arr41[i])
        }
    }   
    return arr42
}

// 5th
function notify(msg) {
    setTimeout(() => alert(msg), 4000);
}

// 6th
array=[-1,-2,1,2,3,4,'12',13.23,1.12];
function sumNaturals(tmpArray) {
    let tmpTotal=0;
    for(let i=0; i<=tmpArray.length; i++)
    {
        if(Number.isInteger(tmpArray[i])&&tmpArray[i]>0)
        {
            tmpTotal+=tmpArray[i];
        }
    }
    return tmpTotal;
}

// 7th
function countChar(){
    let count = 0
    let str = ('there was message which had sent b```y``` m```y``` side')
    // let char1 = "y"
    for (let i=0; i<str.length; i++){
        if(str.charAt(i)=='y'){
            count++
        }
    }
    return count
}


// 1st
// console.log(checkVowels("The quick brown fox"))

// 2nd
// dayCount()

// 3rd
// let arr3=['a',1,'@', '#', 'd', 6.3]
// symb(arr3)

// 4th
// console.log(intersect([1,2,3,4,5,6,7,8,9],[7,8,9,10,11,12,13]))

// 5th
// notify("Sup")

// 6th
// console.log(sumNaturals(array));

// 7th
// console.log(countChar())
